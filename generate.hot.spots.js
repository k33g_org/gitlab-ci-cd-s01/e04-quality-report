const fs = require("fs")

let severityValues = ["info", "minor", "major", "critical", "blocker"]

let hotSpots = [
    {
        description: "🦊",
        fingerprint: "001",
        location: {
            path: "fox.txt",
            lines: {
                begin: 3
            }
        },
        severity: severityValues[2]
    },
    {
        description: "🐼",
        fingerprint: "002",
        location: {
            path: "panda.txt",
            lines: {
                begin: 1
            }
        },
        severity:severityValues[0]
    }

]

fs.writeFileSync("./gl-code-quality-report.json", JSON.stringify(hotSpots, null, 2))
